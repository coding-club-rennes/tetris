local class = require 'middleclass'

local Block = class 'Block'

function Block:initialize(pattern, color)
    self.color = color
    self.height = #pattern
    self.width = 0
    for i = 1, #pattern do
        if pattern[i]:len() > self.width then
            self.width = pattern[i]:len()
        end
    end
    
    self.tiles = {}
    for y = 1, self.height do
        self.tiles[y] = {}
        for x = 1, self.width do
            if pattern[y]:len() < x or pattern[y]:sub(x, x) == ' ' then
                self.tiles[y][x] = nil
            else
                self.tiles[y][x] = color
            end
        end
    end
end

function Block:draw(x, y)
    local r, g, b, a = love.graphics.getColor()
    local backup = {r, g, b, a}
    love.graphics.setColor(self.color)
    for by = 1, self.height do
        for bx = 1, self.width do
            if self.tiles[by][bx] ~= nil then
                love.graphics.rectangle("fill", x + (bx - 1) * 40, y + (by - 1) * 40, 40, 40)
            end
        end
    end
    love.graphics.setColor(backup)
end

return Block