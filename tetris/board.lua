local class = require 'middleclass'

local Board = class 'Board'

function Board:initialize(width, height)
    self.width = width
    self.height = height
    self.tiles = {}
    for y = 1, height do
        self.tiles[y] = {}
        for x = 1, width do
            self.tiles[y][x] = nil
        end
    end
end

-- Permet de savoir si le bloc donné en paramètre superpose un bloc déjà présent sur le plateau
function Board:conflicts(block, x, y)
    if x < 0 or x > self.width - block.width + 1 or y < 0 or y > self.height - block.height + 1 then return true end
    for bx = 1, block.width do
        for by = 1, block.height do
            if block.tiles[by][bx] ~= nil then
                if self.tiles[y + by - 1][x + bx - 1] ~= nil then
                    return true
                end
            end
        end
    end
    return false
end

-- Fusionne le bloc donné avec le plateau
function Board:fuse(block, x, y)
    if Board.conflicts(self, block, x, y) == true then return end
    for bx = 1, block.width do
        for by = 1, block.height do
            self.tiles[y + by - 1][x + bx - 1] = block.tiles[by][bx]
        end
    end
end

-- Dessine le plateau à l'écran
function Board:draw(x, y)
    local r, g, b, a = love.graphics.getColor()
    local backup = {r, g, b, a}
    for bx = 1, self.width do
        for by = 1, self.height do
            if self.tiles[by][bx] ~= nil then
                love.graphics.setColor(self.tiles[by][bx])
                love.graphics.rectangle("fill", x + (bx - 1) * 40, y + (by - 1) * 40, 40, 40)
            end
        end
    end
    love.graphics.setColor(backup)
end

return Board