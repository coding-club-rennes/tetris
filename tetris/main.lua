local Block = require 'block'
local Board = require 'board'

WHITE = {255, 255, 255}
RED = {255, 0, 0}
GREEN = {0, 255, 0}
BLUE = {0, 0, 255}
YELLOW = {255, 255, 0}
MAGENTA = {255, 0, 255}
CYAN = {0, 255, 255}

GameState = {
    score = 0,
    elapsedTime = 0,
    elapsed = "00:00",
    currentTurn = 0,
    currentBlock = nil,
    currentBlockPos = {1, 1},
    nextBlock = nil,
    board = nil,
}

-- Fonction appelée une seule fois au démarrage du jeu
function love.load()
    GameState.board = Board:new(9, 14)
    GameState.currentBlock = Block:new({
        "##",
        "##",
    }, CYAN)
    GameState.nextBlock = Block:new({
        " # ",
        "###",
    }, RED)
end

-- Fonction appelée plusieurs fois par seconde (typiquement 60)
-- Utilisée pour mettre à jour l'état du jeu
function love.update(dt)
    GameState.elapsedTime = GameState.elapsedTime + dt
    GameState.elapsed = string.format("%02d", math.floor(GameState.elapsedTime / 60)) .. ":" .. string.format("%02d", math.floor(GameState.elapsedTime % 60))

    GameState.currentTurn = GameState.currentTurn + dt
    -- Si il s'est écoulé plus d'une seconde
    if GameState.currentTurn > 1 then
        GameState.currentTurn = GameState.currentTurn - 1
        -- On vérifie si on peut descendre le bloc
        if GameState.board:conflicts(GameState.currentBlock, GameState.currentBlockPos[1], GameState.currentBlockPos[2] + 1) then
            -- Si on ne peut pas, on le fusionne avec le plateau à sa position actuelle
            GameState.board:fuse(GameState.currentBlock, GameState.currentBlockPos[1], GameState.currentBlockPos[2])
            -- On procède à la rotation des blocs
            GameState.currentBlock = GameState.nextBlock
            -- On créé le prochain bloc de manière à l'afficher
            GameState.nextBlock = Block:new({
                "#  ",
                "###",
            }, BLUE)
            -- On réinitialise la position du bloc
            GameState.currentBlockPos = {1, 1}
        else
            -- Sinon on le descend d'un cran'
            GameState.currentBlockPos[2] = GameState.currentBlockPos[2] + 1
        end
    end

end

function love.keypressed(key, scancode, isrepeat)
    print("La touche '" .. key .. "' a été pressée")
end

function love.keyreleased(key, scancode, isrepeat)
    print("La touche '" .. key .. "' a été relachée")
end

-- Fonction appelée plusieurs fois par seconde (typiquement 60)
-- Utilisée pour dessiner l'état du jeu à l'écran
function love.draw()
    -- Dessine les "boîtes" du jeu
    love.graphics.rectangle("line", 20,  20,  360, 560, 4, 4)
    love.graphics.rectangle("line", 420, 20,  160, 160, 4, 4)
    love.graphics.rectangle("line", 420, 220, 160, 360, 4, 4)

    -- Affiche le temps écoulé depuis le début de la partie
    love.graphics.printf("TIME", 440, 240, 120, "left")
    love.graphics.printf(GameState.elapsed, 440, 240, 120, "right")

    -- Affiche le score du joueur
    love.graphics.printf("SCORE", 440, 260, 120, "left")
    love.graphics.printf(GameState.score, 440, 260, 120, "right")

    -- Affiche le plateau
    GameState.board:draw(20, 20)
    -- Affiche le bloc actuel
    GameState.currentBlock:draw(20 + (GameState.currentBlockPos[1] - 1) * 40, 20 + (GameState.currentBlockPos[2] - 1) * 40)
    -- Affiche le prochain bloc
    GameState.nextBlock:draw(420, 20)
end
